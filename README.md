<!--
  Copyright (C) 2023  CismonX <admin@cismon.net>

  Copying and distribution of this file, with or without modification, are
  permitted in any medium without royalty, provided the copyright notice and
  this notice are preserved. This file is offered as-is, without any warranty.
-->


Introduction
------------

  ARIF is an input method framework for GNU Readline.

  ARIF is free software, distributed under the terms of the GNU General Public
  License, either version 3, or any later version of the license.  For more
  information, see the file 'COPYING'.


Getting Started
---------------

  See 'INSTALL.md' for instructions on how to build and install ARIF.

  See the files under 'doc/' for documentation.

  For more information, visit the project homepage: <https://nongnu.org/arif>.
